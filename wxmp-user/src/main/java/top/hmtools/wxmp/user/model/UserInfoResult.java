package top.hmtools.wxmp.user.model;

import java.util.Date;
import java.util.List;

import top.hmtools.wxmp.core.model.ErrcodeBean;

/**
 * Auto-generated: 2019-08-14 17:41:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UserInfoResult extends ErrcodeBean {

	private String city;
	private String country;
	private int groupid;
	private String headimgurl;
	private String language;
	private String nickname;
	private String openid;
	private String province;
	private long qr_scene;
	private String qr_scene_str;
	private String remark;
	private int sex;
	private int subscribe;
	private String subscribe_scene;
	private long subscribe_time;
	private List<Long> tagid_list;
	private String unionid;

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry() {
		return country;
	}

	public void setGroupid(int groupid) {
		this.groupid = groupid;
	}

	public int getGroupid() {
		return groupid;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getOpenid() {
		return openid;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvince() {
		return province;
	}

	public void setQr_scene(long qr_scene) {
		this.qr_scene = qr_scene;
	}

	public long getQr_scene() {
		return qr_scene;
	}

	public void setQr_scene_str(String qr_scene_str) {
		this.qr_scene_str = qr_scene_str;
	}

	public String getQr_scene_str() {
		return qr_scene_str;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getSex() {
		return sex;
	}

	public void setSubscribe(int subscribe) {
		this.subscribe = subscribe;
	}

	public int getSubscribe() {
		return subscribe;
	}

	public void setSubscribe_scene(String subscribe_scene) {
		this.subscribe_scene = subscribe_scene;
	}

	public String getSubscribe_scene() {
		return subscribe_scene;
	}

	public void setSubscribe_time(long subscribe_time) {
		this.subscribe_time = subscribe_time;
	}

	public long getSubscribe_time() {
		return subscribe_time;
	}


	public List<Long> getTagid_list() {
		return tagid_list;
	}

	public void setTagid_list(List<Long> tagid_list) {
		this.tagid_list = tagid_list;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getUnionid() {
		return unionid;
	}

	@Override
	public String toString() {
		return "UserInfoResult [city=" + city + ", country=" + country + ", groupid=" + groupid + ", headimgurl="
				+ headimgurl + ", language=" + language + ", nickname=" + nickname + ", openid=" + openid
				+ ", province=" + province + ", qr_scene=" + qr_scene + ", qr_scene_str=" + qr_scene_str + ", remark="
				+ remark + ", sex=" + sex + ", subscribe=" + subscribe + ", subscribe_scene=" + subscribe_scene
				+ ", subscribe_time=" + subscribe_time + ", tagid_list=" + tagid_list + ", unionid=" + unionid
				+ ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}

}