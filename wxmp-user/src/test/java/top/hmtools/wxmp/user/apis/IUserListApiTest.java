package top.hmtools.wxmp.user.apis;

import org.junit.Test;

import top.hmtools.wxmp.BaseTest;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.user.model.UserListParam;
import top.hmtools.wxmp.user.model.UserListResult;

public class IUserListApiTest extends BaseTest {
	
	protected WxmpSession wxmpSession;
	private IUserListApi iUserListApi ;
	

	@Test
	public void testGetUserList() {
		UserListParam userListParam = new UserListParam();
		UserListResult userList = this.iUserListApi.getUserList(userListParam);
		System.out.println(userList);
	}

}
