package top.hmtools.wxmp.user.apis;

import org.junit.Test;

import top.hmtools.wxmp.BaseTest;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.model.RemarkParam;

public class IRemarkApiTest extends BaseTest {
	
	protected WxmpSession wxmpSession;
	private IRemarkApi remarkApi;
	

	@Test
	public void testUpdateRemark() {
		RemarkParam remarkParam = new RemarkParam();
		remarkParam.setOpenid("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		remarkParam.setRemark("wahahahaahaah");
		ErrcodeBean updateRemark = this.remarkApi.updateRemark(remarkParam);
		System.out.println(updateRemark);
	}

}
