package top.hmtools.wxmp.user.apis;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import top.hmtools.wxmp.BaseTest;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.user.model.BatchUserInfoParam;
import top.hmtools.wxmp.user.model.BatchUserInfoResult;
import top.hmtools.wxmp.user.model.UserInfoParam;
import top.hmtools.wxmp.user.model.UserInfoResult;

public class IUnionIDApiTest extends BaseTest{
	
	protected WxmpSession wxmpSession;
	private IUnionIDApi iUnionIDApi ;
	

	@Test
	public void testGetUserInfo() {
		UserInfoParam userInfoParam = new UserInfoParam();
		userInfoParam.setOpenid("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		UserInfoResult userInfo = this.iUnionIDApi.getUserInfo(userInfoParam);
		System.out.println(userInfo);
	}

	@Test
	public void testGetBatchUserInfo() {
		BatchUserInfoParam batchUserInfoParam = new BatchUserInfoParam();
		
		UserInfoParam userInfoParam = new UserInfoParam();
		userInfoParam.setOpenid("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		
		List<UserInfoParam> user_list = new ArrayList<>();
		user_list.add(userInfoParam);
		batchUserInfoParam.setUser_list(user_list);
		BatchUserInfoResult batchUserInfo = this.iUnionIDApi.getBatchUserInfo(batchUserInfoParam);
		System.out.println(batchUserInfo);
	}

}
