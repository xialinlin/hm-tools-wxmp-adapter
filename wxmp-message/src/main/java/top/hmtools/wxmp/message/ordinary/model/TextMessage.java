package top.hmtools.wxmp.message.ordinary.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 文本消息
 * {@code 
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[fromUser]]></FromUserName>
	<CreateTime>1348831860</CreateTime>
	<MsgType><![CDATA[text]]></MsgType>
	<Content><![CDATA[this is a test]]></Content>
	<MsgId>1234567890123456</MsgId>
</xml>
 * }
 * 
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.text)
public class TextMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8090390109332033707L;
	
	/**
	 * 文本消息内容
	 */
	@XStreamAlias("Content")
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "TextMessage [content=" + content + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName
				+ ", createTime=" + createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}

	
	

}
