package top.hmtools.wxmp.message.reply.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseMessage;

/**
 * 回复语音消息
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>
  <FromUserName><![CDATA[fromUser]]></FromUserName>
  <CreateTime>12345678</CreateTime>
  <MsgType><![CDATA[voice]]></MsgType>
  <Voice>
    <MediaId><![CDATA[media_id]]></MediaId>
  </Voice>
</xml>
 * }
 * @author hybo
 *
 */
public class ReplyVoiceMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4508385182429601491L;

	/**
	 * 回复语音消息 中 的语音素材信息
	 */
	@XStreamAlias("Voice")
	private ReplyVoice voice;

	public ReplyVoice getVoice() {
		return voice;
	}

	public void setVoice(ReplyVoice voice) {
		this.voice = voice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	
}
