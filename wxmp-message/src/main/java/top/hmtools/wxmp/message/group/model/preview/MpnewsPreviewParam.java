package top.hmtools.wxmp.message.group.model.preview;

/**
 * 预览--图文
 * @author HyboWork
 *
 */
public class MpnewsPreviewParam extends BasePreviewParam {

	private MediaId mpnews;

	public MediaId getMpnews() {
		return mpnews;
	}

	public void setMpnews(MediaId mpnews) {
		this.mpnews = mpnews;
	}

	@Override
	public String toString() {
		return "MpnewsPreviewParam [mpnews=" + mpnews + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
	
	
}
