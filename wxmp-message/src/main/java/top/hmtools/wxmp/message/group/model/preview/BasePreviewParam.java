package top.hmtools.wxmp.message.group.model.preview;

import top.hmtools.wxmp.message.group.model.tagGroupSend.TagMsgType;

/**
 * Auto-generated: 2019-08-27 16:41:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class BasePreviewParam {

	protected TagMsgType msgtype;
	
	protected String touser;
	
	protected String towxname;

	public TagMsgType getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(TagMsgType msgtype) {
		this.msgtype = msgtype;
	}

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTowxname() {
		return towxname;
	}

	public void setTowxname(String towxname) {
		this.towxname = towxname;
	}

	@Override
	public String toString() {
		return "BasePreviewParam [msgtype=" + msgtype + ", touser=" + touser + ", towxname=" + towxname + "]";
	}


}