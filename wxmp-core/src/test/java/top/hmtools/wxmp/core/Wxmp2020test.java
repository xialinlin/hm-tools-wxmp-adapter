package top.hmtools.wxmp.core;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

import top.hmtools.javabean.models.BbbBean;
import top.hmtools.wxmp.core.configuration.AppIdSecretBox;
import top.hmtools.wxmp.core.configuration.AppIdSecretPair;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;
import top.hmtools.wxmp.core.wxmpApiDemo.WApiAAA;

public class Wxmp2020test {

	/**
	 * 测试 invoke 中获取反射method中的相关信息
	 */
	@Test
	public void testAAA(){
		WxmpConfiguration config = new WxmpConfiguration();
		
		WxmpSessionFactory wxmpSessionFactory2020 = WxmpSessionFactoryBuilder.build(config);
		WxmpSession wxmpSession2020 = wxmpSessionFactory2020.openSession();
		
		WApiAAA wApiAAA = wxmpSession2020.getMapper(WApiAAA.class);
		wApiAAA.doAAA("aaaa1111", true, new HashMap<String,String>());
		wApiAAA.doBBB("bbbb222", true, new HashMap<String,String>());
		try {
			wApiAAA.doCCC("cc3333", true, new HashMap<String,String>(), IOUtils.toInputStream("aaabbbccc", "utf-8"), new BbbBean());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	@Test
	public void testDefaultParameterNameDiscoverer(){
		//需要 asm 框架
		LocalVariableTableParameterNameDiscoverer  dpnd = new LocalVariableTableParameterNameDiscoverer ();
		Method[] declaredMethods = WApiAAA.class.getDeclaredMethods();
		for(Method mt:declaredMethods){
			String[] parameterNames = dpnd.getParameterNames(mt);
			if(parameterNames == null){
				break;
			}
			for(String str:parameterNames){
				System.out.println(str);
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
