package top.hmtools.wxmp.core;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

/**
 * 会话工厂
 * @author HyboWork
 *
 */
public abstract class WxmpSessionFactory {

	private WxmpConfiguration configuration2020;
	
	public WxmpSessionFactory( WxmpConfiguration configuration){
		this.configuration2020 = configuration;
	}

	public WxmpConfiguration getConfiguration2020() {
		return configuration2020;
	}

	public void setConfiguration2020(WxmpConfiguration configuration2020) {
		this.configuration2020 = configuration2020;
	}
	
	/**
	 * 获取会话
	 * @return
	 */
	public abstract WxmpSession openSession();
}
