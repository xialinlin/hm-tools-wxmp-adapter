package top.hmtools.wxmp.core;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public class WxmpSessionFactoryBuilder {

	/**
	 * 获取会话工厂
	 * @param configuration
	 * @return
	 */
	public static WxmpSessionFactory build(WxmpConfiguration configuration){
		if(configuration == null){
			throw new RuntimeException("全局配置信息不能为null");
		}
//		if(configuration.getAppIdSecretBox() == null){
//			throw new RuntimeException("没有配置获取微信appid、appsecret信息的数据盒子");
//		}
		
		if(configuration.getAccessTokenHandle() == null){
			throw new RuntimeException("没有配置获取微信access token信息的中间件");
		}
		
		WxmpSessionFactory result = configuration.getWxmpSessionFactory2020();
		if(result != null){
			return result;
		}else{
			return new DefaultWxmpSessionFactory(configuration);
		}
	}
}
