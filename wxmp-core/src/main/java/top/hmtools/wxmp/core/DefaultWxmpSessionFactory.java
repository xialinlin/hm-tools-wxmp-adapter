package top.hmtools.wxmp.core;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public class DefaultWxmpSessionFactory extends WxmpSessionFactory {

	public DefaultWxmpSessionFactory(WxmpConfiguration configuration) {
		super(configuration);
	}

	@Override
	public WxmpSession openSession() {
		DefaultWxmpSession result = new DefaultWxmpSession(this.getConfiguration2020());
		return result;
	}

}
