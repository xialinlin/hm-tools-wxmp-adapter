package top.hmtools.wxmp.core;

import java.util.HashMap;

import org.apache.http.entity.mime.MultipartEntityBuilder;

import top.hmtools.wxmp.core.httpclient.HmHttpClientTools;

public class RequestParamConvertBlank implements RequestParamConvert {

	@Override
	public MultipartEntityBuilder convert(HashMap<String, Object> params) {
		return HmHttpClientTools.buildMultipartEntity(params);
	}

}
