package top.hmtools.wxmp.menu.apis;

import org.junit.Test;

import com.alibaba.fastjson.JSON;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.menu.BaseTest;
import top.hmtools.wxmp.menu.models.simple.Button;
import top.hmtools.wxmp.menu.models.simple.CurrentSelfMenuInfoBean;
import top.hmtools.wxmp.menu.models.simple.MenuBean;
import top.hmtools.wxmp.menu.models.simple.MenuWapperBean;

public class IMenuApiTest extends BaseTest{
	
	private IMenuApi menuApi ;
	
	@Test
	public void testcreateMenu(){
		
		String baseUrl = "hm.hn.cn";
		
		//底部第一个主按钮
		Button bbGuanwang = new Button();
		bbGuanwang.setName("官网").setType("view").setUrl("http://m.hybo.net/main/index/index.html");
		
		Button bbDongTai = new Button();
		bbDongTai.setName("动态").setType("view").setUrl("http://"+baseUrl+"/main/news/index.html");
		
		Button bbVideo = new Button();
		bbVideo.setName("视频").setType("view").setUrl("http://"+baseUrl+"/main/news/video.html");
		
		Button buttonBeanIndex = new Button();
		buttonBeanIndex.setName("哈哈");
		buttonBeanIndex.addSubButton(bbGuanwang,bbDongTai,bbVideo);
		
		MenuBean menuBean = new MenuBean();
		menuBean.addButton(buttonBeanIndex);
		ErrcodeBean createMenu = this.menuApi.createMenu(menuBean);
		System.out.println(createMenu);
	}

	/**
	 * 测试获取自定义菜单目录
	 */
	@Test
	public void testGetMenu() {
		
		MenuWapperBean menu = menuApi.getMenu();
		System.out.println(JSON.toJSONString(menu));
	}
	
	
	@Test
	public void testdeleteAllMenu(){
		ErrcodeBean deleteAllMenu = this.menuApi.deleteAllMenu();
		System.out.println(deleteAllMenu);
	}
	
	
	@Test
	public void testgetCurrentSelfMenuInfoUri(){
		CurrentSelfMenuInfoBean currentSelfMenuInfoUri = this.menuApi.getCurrentSelfMenuInfo();
		System.out.println(JSON.toJSONString(currentSelfMenuInfoUri));
	}


	@Override
	public void initSub() {
		this.menuApi = this.wxmpSession.getMapper(IMenuApi.class);		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
