package top.hmtools.wxmp.menu;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.access_handle.DefaultAccessTokenHandle;
import top.hmtools.wxmp.core.configuration.AppIdSecretBox;
import top.hmtools.wxmp.core.configuration.AppIdSecretPair;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public abstract class BaseTest {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected WxmpSession wxmpSession;
	
	protected WxmpSessionFactory factory;

	@Before
	public void init(){
		//TODO 必要时，可以对 httpclient进行设置
//		HmHttpClientFactoryHandle.setConnectionKeepAliveStrategy(connectionKeepAliveStrategy);
		//…………
		
		WxmpConfiguration configuration = new WxmpConfiguration();
		
		//设置 存储appid，appsecret 数据对 的盒子
		AppIdSecretBox appIdSecretBox = new AppIdSecretBox() {
			
			@Override
			public AppIdSecretPair getAppIdSecretPair() {
				AppIdSecretPair appIdSecretPair = new AppIdSecretPair();
				appIdSecretPair.setAppid(AppId.appid);
				appIdSecretPair.setAppsecret(AppId.appsecret);
				return appIdSecretPair;
			}
		};
		
		//设置 获取access token 的中间件
		DefaultAccessTokenHandle accessTokenHandle = new DefaultAccessTokenHandle(appIdSecretBox);
		configuration.setAccessTokenHandle(accessTokenHandle);
		
		this.factory = WxmpSessionFactoryBuilder.build(configuration);
		this.wxmpSession = this.factory.openSession();
		
		this.initSub();
	}
	
	
	
	public abstract void initSub();
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
